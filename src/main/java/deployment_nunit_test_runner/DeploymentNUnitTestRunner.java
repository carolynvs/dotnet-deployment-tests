package deployment_nunit_test_runner;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.build.logger.interceptors.ErrorMemorisingInterceptor;
import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.deployments.execution.DeploymentTaskContext;
import com.atlassian.bamboo.deployments.execution.DeploymentTaskType;
import com.atlassian.bamboo.plugin.dotnet.tests.nunit.NUnitRunnerTaskType;
import com.atlassian.bamboo.plugin.dotnet.tests.nunit.NUnitTestReportCollector;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.utils.process.ExternalProcess;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.tools.ant.types.Commandline;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class DeploymentNUnitTestRunner implements DeploymentTaskType {

    // ---------------------------------------------------------------------------------------------------- Dependencies

    private final ProcessService processService;
    private final CapabilityContext capabilityContext;
    private final EnvironmentVariableAccessor environmentVariableAccessor;

    // ---------------------------------------------------------------------------------------------------- Constructors


    public DeploymentNUnitTestRunner(final ProcessService processService,
                                     final CapabilityContext capabilityContext, final EnvironmentVariableAccessor environmentVariableAccessor) {
        this.processService = processService;
        this.capabilityContext = capabilityContext;
        this.environmentVariableAccessor = environmentVariableAccessor;
    }

    @Override
    public TaskResult execute(DeploymentTaskContext taskContext) throws TaskException {
        final ErrorMemorisingInterceptor errorLines = new ErrorMemorisingInterceptor();
        BuildLogger logger = taskContext.getBuildLogger();
        logger.getInterceptorStack().add(errorLines);

        try {
            final ConfigurationMap configuration = taskContext.getConfigurationMap();

            final String testFiles = configuration.get(NUnitRunnerTaskType.TEST_FILES);
            Preconditions.checkNotNull(testFiles, "One or more test files must be specified");

            final String resultsFile = configuration.get(NUnitRunnerTaskType.RESULTS_FILE);
            Preconditions.checkNotNull(resultsFile, "Results File Name");

            final String label = configuration.get(NUnitRunnerTaskType.LABEL);
            Preconditions.checkNotNull(label, "You must select an NUnit Runner before this task can execute.");

            final Capability capability = capabilityContext.getCapabilitySet().getCapability(NUnitRunnerTaskType.NUNIT_CAPABILITY_PREFIX + "." + label);
            Preconditions.checkNotNull(capability, "Capability");

            final File nunitConsoleExe = new File(capability.getValue());
            Preconditions.checkArgument(nunitConsoleExe.exists(), "NUnit Console Executable not found");

            final List<String> command = Lists.newArrayList(nunitConsoleExe.getAbsolutePath());

            final String testsToRun = configuration.get(NUnitRunnerTaskType.TESTS_TO_RUN);
            if (StringUtils.isNotBlank(testsToRun)) {
                command.add("-run=" + testsToRun);
            }

            command.add(testFiles);
            command.add("-xml=" + resultsFile);

            final String includedCategories = configuration.get(NUnitRunnerTaskType.INCLUDED_TEST_CATEGORIES);
            if (StringUtils.isNotBlank(includedCategories)) {
                command.add("-include=" + includedCategories);
            }

            final String excludedCategories = configuration.get(NUnitRunnerTaskType.EXCLUDED_TEST_CATEGORIES);
            if (StringUtils.isNotBlank(excludedCategories)) {
                command.add("-exclude=" + excludedCategories);
            }

            final String environment = configuration.get(NUnitRunnerTaskType.ENVIRONMENT);

            final Map<String, String> env = environmentVariableAccessor.splitEnvironmentAssignments(environment);

            final String commandLineOptions = configuration.get(NUnitRunnerTaskType.COMMAND_OPTIONS);
            if (StringUtils.isNotBlank(commandLineOptions)) {
                command.addAll(Arrays.asList(Commandline.translateCommandline(commandLineOptions)));
            }

            final ExternalProcess externalProcess = processService.executeExternalProcess(taskContext,
                    new ExternalProcessBuilder()
                            .env(env)
                            .command(command)
                            .workingDirectory(taskContext.getWorkingDirectory()));

            TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(taskContext).checkReturnCode(externalProcess);

            NUnitTestReportCollector reportCollector = new NUnitTestReportCollector();
            TestCollectionResult testResults = reportCollector.collect(new File(taskContext.getWorkingDirectory(), resultsFile));
            int failedTestResultsCount = testResults.getFailedTestResults().size();
            int successfulTestResultsCount = testResults.getSuccessfulTestResults().size();
            int skippedTestResultsCount = testResults.getSkippedTestResults().size();
            if (failedTestResultsCount > 0)
            {
                logger.addBuildLogEntry(String.format("Failing task since %d failing test cases were found.", failedTestResultsCount));
                taskResultBuilder.failed();
            }
            else if (successfulTestResultsCount == 0 && skippedTestResultsCount == 0)
            {
                logger.addBuildLogEntry("Failing task since test cases were expected but none were found.");
                taskResultBuilder.failed();
            }

            return taskResultBuilder.build();
        } catch (Exception ex) {
            logger.addErrorLogEntry("DeploymentNUnitTestRunner: Error running NUnitRunnerTaskType.execute", ex);
            return TaskResultBuilder.newBuilder(taskContext).failedWithError().build();
        } finally {
            taskContext.getDeploymentContext().getCurrentResult().addBuildErrors(errorLines.getErrorStringList());
        }
    }
}