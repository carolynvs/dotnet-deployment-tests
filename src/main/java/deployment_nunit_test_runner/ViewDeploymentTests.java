package deployment_nunit_test_runner;

import com.atlassian.bamboo.deployments.results.DeploymentResult;
import com.atlassian.bamboo.deployments.results.service.DeploymentResultService;
import com.atlassian.plugin.web.model.WebPanel;
import com.google.common.collect.ImmutableMap;
import org.apache.struts2.ServletActionContext;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.util.Map;

public class ViewDeploymentTests implements WebPanel
{
    private DeploymentResultService deploymentResultService;

    public ViewDeploymentTests(DeploymentResultService deploymentResultService)
    {
        this.deploymentResultService = deploymentResultService;
    }

    @Override
    public String getHtml(Map<String, Object> context)
    {
        String deploymentResultIdParam = ServletActionContext.getRequest().getParameter("deploymentResultId");
        long deploymentResultId = Long.valueOf(deploymentResultIdParam).longValue();
        DeploymentResult deploymentResult = deploymentResultService.getDeploymentResult(deploymentResultId);
        String workingDirectory = deploymentResult.getCustomData().get("build.working.directory");
        File testResults = new File(workingDirectory, "TestResult.xml");

        return testResults.getPath();
    }

    @Override
    public void writeHtml(Writer writer, Map<String, Object> context) throws IOException
    {
        writer.write(getHtml(context));
    }
}
